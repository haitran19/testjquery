﻿$(document).ready(function () {
    //ẩn class đi
    $('.content').slideUp();
    //sự kiện click
    $('.box h3').click(function (event) {
        $(this).next().slideToggle();
        $(this).toggleClass('xanh');
    });

    // lưới gird ảnh
    $('.content2').isotope({
        itemSelector: '.item',
        layoutModel: 'fitRows'
    });

    //kiểm soát phím gõ vào
    $('.tukhoa').keypress(function (event) {
        if (event.keyCode == "13") { //13 là phím enter
            giatri = $('.tukhoa').val(); //lấy giá trị được nhập vào
            giatri = "." + giatri;
            $('.content2').isotope({ filter: giatri });
        }
    });

    //cứ chạy hàm sau khoảng 2 giây
    // thay thế cho việc gõ enter ở trên
    var thoigian = setInterval(function () {
        giatri = $('.tukhoa').val(); //lấy giá trị được nhập vào
        giatri = "." + giatri;
        $('.content2').isotope({ filter: giatri });
    }, 200);

    //sự kiện click nút
    $('nav ul li a').click(function (event) {
        var danhmuc = $(this).data('img');
        var name = $(this).text();

        danhmuc = "." + danhmuc;
        if (danhmuc == ".all") {
            $('h1').text('Tất cả các ảnh');
            $('.content2').isotope({ filter: '*' }); // lọc bằng filter
        }
        else {
            $('h1').text(name);
            $('.content2').isotope({ filter: danhmuc });
        }
        return false;
    });

    $('#a').click(function (e) {
        e.preventDefault();
        ('.divone').removeClass('hide');
    })
});